'use strict';

var routes = require('./routes');
var requires = [];

module.exports = angular.module('app.linux.about', requires)
  .config(routes);

// controllers

// directives

// services
