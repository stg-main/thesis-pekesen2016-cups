'use strict';

//@ngInject
module.exports = function routes($stateProvider) {

  $stateProvider
    .state('linux.about', {
      url: '/about',
      template: require('../about/about.html')
    })
};
