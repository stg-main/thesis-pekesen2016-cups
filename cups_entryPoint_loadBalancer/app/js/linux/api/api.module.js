'use strict';

var requires = [];

module.exports = angular.module('app.linux.api', requires);

// controllers

// directives

// services
require('./authresource.service');
require('./userresource.service');
require('./participantrescource.service');

// routes
