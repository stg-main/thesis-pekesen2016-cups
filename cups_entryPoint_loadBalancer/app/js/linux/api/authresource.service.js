'use strict';

var component = require('./api.module');

component.service('LauthResource', authResourceService);

function authResourceService($resource, $httpParamSerializer, Luser, $state) {
  var SignIn = $resource('/api/v1/signin/:email/:password', {}, {
    login: {
      method: 'POST',
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }
  });
  var SignInVM = $resource('/api/v1/linux/vm/signin/:email/:password', {}, {
    login: {
      method: 'POST',
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }
  });
  var Register = $resource('/api/v1/signup/:email/:password', {}, {
    login: {
      method: 'POST',
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }
  });
  var LogOut = $resource('/api/v1/logout');

  return {
    signIn: signIn,
    logOut: logOut,
    signUp: signUp,
    signInVM: signInVM
  };

  function signInVM(email, password) {
    // @httpParamSerializer converts json to application/x-www-form-urlencoded format
    return SignInVM.login($httpParamSerializer({
      email: email,
      password: password
    })).$promise
      .then((res) => {
        return Luser.loadState()
          .then(() => {
            return res;
          });
      })
      .catch((res) => {
        if (res.status === 404) {
          window.alert('Username: ' + email + ' not found!');
        } else {
          window.alert('Something went wrong. Please try again later!');
        }
      });
  }

  function signIn(email, password) {
    // @httpParamSerializer converts json to application/x-www-form-urlencoded format
    return SignIn.login($httpParamSerializer({
      email: email,
      password: password
    })).$promise
      .then((res) => {
        return Luser.loadState()
          .then(() => {
            return res;
          });
      })
      .catch((res) => {
        if (res.status === 404) {
          window.alert('Username: ' + email + ' not found!');
        } else {
          window.alert('Something went wrong. Please try again later!');
        }
      });
  }

  function signUp(email, password) {
    // @httpParamSerializer converts json to application/x-www-form-urlencoded format
    return Register.login($httpParamSerializer({
      email: email,
      password: password
    })).$promise
      .then((res) => {
        return Luser.loadState()
          .then(() => {
            return res;
          });
      });
  }

  function logOut() {
    return LogOut.save().$promise
      .then((res) => {
        return Luser.loadState()
          .then(() => {
            Luser.clearLocal();
            $state.go('linux.home');
            return res;
          });
      });
  }
}

