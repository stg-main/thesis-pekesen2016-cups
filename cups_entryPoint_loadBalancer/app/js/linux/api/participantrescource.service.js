'use strict';

var component = require('./api.module');

component.service('LparticipantResource', participantResource);

function participantResource($resource, Luser) {
  var Participant = $resource('/api/v1/linux/participant/vm');
  var EndStudy = $resource('/api/v1/linux/participant/end');
  var Feedback = $resource('/api/v1/linux/participant/feedback');
  var CancelStudy = $resource('/api/v1/linux/participant/cancel');
  var Interactions = $resource('/api/v1/linux/participant/focus');
  var Client = $resource('/api/v1/linux/participant/client');

  return {
    getVM: getVM,
    endStudy: endStudy,
    submitFeedback: submitFeedback,
    cancelStudy: cancelStudy,
    sendInteractions: sendInteractions,
    sendClientInfo: sendClientInfo
  };

  function getVM() {
    return Participant.get().$promise
      .then((res) => {
        Luser.loadState();
        return res;
      });
  }

  function endStudy() {
    return EndStudy.save().$promise
      .then((res) => {
        return Luser.loadState()
          .then(() => {
            return res
          });
      });
  }

  function cancelStudy(cancelInfo) {
    return CancelStudy.save({cancelInfo: cancelInfo}).$promise
      .then((res) => {
        return Luser.loadState()
          .then(() => {
            return res
          });
      });
  }

  function submitFeedback(feedback) {
    return Feedback.save({feedback: feedback}).$promise
      .then((res) => {
        return Luser.loadState()
          .then(() => {
            return res
          });
      });
  }

  function sendInteractions() {
    return Interactions.save({interactions: Luser.introWindowInteractions}).$promise
      .then(() => {
        return Luser.resetInteractions();
      });
  }

  function sendClientInfo(clientInfo) {
    return Client.save({clientInfo: clientInfo}).$promise;
  }
}

