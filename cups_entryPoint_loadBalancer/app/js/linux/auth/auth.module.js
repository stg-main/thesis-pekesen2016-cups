'use strict';

var requires = [
  'app.linux.api'
];

module.exports = angular.module('app.linux.auth', requires);

// controllers

// directives

// services
require('./crypt.service');
require('./user.service');

// routes
