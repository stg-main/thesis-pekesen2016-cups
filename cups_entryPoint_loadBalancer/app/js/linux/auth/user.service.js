'use strict';

var _ = require('lodash');

var component = require('./auth.module');

class userService {
  constructor(LuserResource, Lcrypt, $state) {
    this.crypt = Lcrypt;
    this.user = this.getLocal();

    this.auth = LuserResource;
    this.state = $state;

    this.introWindowInteractions = {
      focusGain: [],
      focusLost: []
    };

    this.loadState();

  }

  loadState() {
    return this.auth.getState().then((user) => {
      this.user = user;
      this.setLocal(this.user);
    })
      .catch(() => {
        this.user = {
          email: '',
          id: '',
          vm: {},
          linux: {
            vm: {}
          }
        };
      });
  }

  resetInteractions() {
    this.introWindowInteractions = {
      focusGain: [],
      focusLost: []
    };
  }

  isClientInfoAvailable() {
    return _.get(this.user, 'linux.clientInfo', false) ? true : false;
  }

  isCancelled() {
    return _.get(this.user, 'linux.cancelled', false);
  }

  isInsideVM() {
    return this.state.includes('linux.vm');
  }

  loggedIn() {
    return this.user.email.length > 0;
  }

  finishedStudy() {
    return _.get(this.user, 'linux.finishedStudy', false);
  }

  finishedFeedback() {
    return _.get(this.user, 'linux.finishedFeedback', false);
  }

  getLocal() {
    var user = window.localStorage.getItem('cups-user');
    if (user) {
      return this.crypt.decrypt(user);
    } else {
      return {
        email: '',
        id: ''
      };
    }
  }

  clearLocal() {
    window.localStorage.removeItem('cups-user');
  }

  setLocal(user) {
    window.localStorage.setItem('cups-user', this.crypt.encrypt(user));
  }

}

component.service('Luser', userService);
