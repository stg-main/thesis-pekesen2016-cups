'use strict';

var components = require('./feedback.module');

var participant, state;

class FeedbackController {
  constructor($state, Luser, LparticipantResource) {
    if (!Luser.loggedIn()) {
      $state.go('linux.home');
    }
    if (!Luser.finishedStudy()) {
      window.alert('You have to finish the evaluation first before giving feedback!');
      $state.go('linux.studyintro');
    }
    if (Luser.finishedFeedback()) {
      $state.go('linux.thankyou');
    }

    participant = LparticipantResource;
    state = $state;
  }

  submit() {
    participant.submitFeedback(this.feedback)
      .then(() => {
        state.go('linux.thankyou');
      });
  }
}

components.controller('LFeedbackController', FeedbackController);
