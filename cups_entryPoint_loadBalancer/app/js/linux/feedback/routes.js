'use strict';

//@ngInject
module.exports = function routes($stateProvider) {

  $stateProvider
    .state('linux.vm.feedback', {
      url: '/feedback',
      template: require('../feedback/feedback.html'),
      controller: 'LFeedbackController',
      controllerAs: 'vm',
      bindToController: true
    })
};
