'use strict';

var component = require('./home.module');

class HomeController {
  constructor($state, Luser) {
    this.$state = $state;
    this.user = Luser;
  }
  
  go(state) {
    this.$state.go(state);
  }
}

component.controller('LHomeController', HomeController);


