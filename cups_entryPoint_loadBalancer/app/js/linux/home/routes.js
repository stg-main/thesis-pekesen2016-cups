'use strict';

//@ngInject
module.exports = function routes($stateProvider) {

  $stateProvider
    .state('linux.home', {
      url: '/home',
      template: require('../home/home.html'),
      controller: 'LHomeController',
      controllerAs: 'vm',
      bindToController: true
    })
};
