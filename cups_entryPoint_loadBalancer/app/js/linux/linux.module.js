'use strict';

var routes = require('./routes');

require('./api');
require('./auth');
require('./signup');
require('./signinVM');
require('./about');
require('./study');
require('./studyintro');
require('./feedback');
require('./thankyou');
require('./home');
require('./navigation');

var requires = [
  'app.linux.home',
  'app.linux.api',
  'app.linux.auth',
  'app.linux.about',
  'app.linux.signup',
  'app.linux.signinVM',
  'app.linux.studyintro',
  'app.linux.study',
  'app.linux.feedback',
  'app.linux.thankyou',
  'app.linux.navigation'
];

module.exports = angular.module('app.linux', requires)
  .config(routes);
