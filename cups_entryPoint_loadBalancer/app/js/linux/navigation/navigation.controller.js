var component = require('./navigation.module');
var _ = require('lodash');

class NavigationController {
  constructor(LauthResource, Luser, $state) {
    this.user = Luser;
    this.auth = LauthResource;
    this.$state = $state;
  }
  
  isActive(stateName) {
    return this.$state.is(stateName) ? 'active' : '';
  }

  login() {
    if (!this.user.loggedIn()) {
      if (_.get(this, 'email.length', '') > 0 && _.get(this, 'password.length', '') > 0) {
        this.auth.signIn(this.email, this.password)
          .then(() => {
            this.email = '';
            this.password = '';
            
            this.$state.go('linux.studyintro');
          });
      }
    }
  }

  logout() {
    if (this.user.loggedIn()) {
      this.auth.logOut();
    }
  }
}

component.controller('LNavigationController', NavigationController);
