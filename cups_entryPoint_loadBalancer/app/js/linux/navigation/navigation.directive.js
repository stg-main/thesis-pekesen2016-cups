'use sctrict';

var component = require('./navigation.module');

component.directive('lnavigation', navigation);

function navigation() {
  return {
    restrict: 'E',
    template: require('./navigation.cache.html'),
    controller: 'LNavigationController',
    controllerAs: 'vm'
  }
}
