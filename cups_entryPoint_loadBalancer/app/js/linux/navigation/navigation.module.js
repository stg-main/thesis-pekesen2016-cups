var requires = [
  'app.linux.api'
];

module.exports = angular.module('app.linux.navigation', requires);


// controllers
require('./navigation.controller');

// directives
require('./navigation.directive');

// services
