'use strict';

//@ngInject
module.exports = function routes($stateProvider) {

  $stateProvider
    .state('linux', {
      abstract: true,
      url: '/LkUg7u2MS',
      template: '<lnavigation ng-hide="isInsideVM()"></lnavigation> <div class="content col-md-8 col-sm-12" ui-view></div>  <img class="pull-right logo col-md-3 hidden-sm hidden-xs" ng-src="dist/images/cupsLogo.png">',
      controller: function($scope, Luser) {
        $scope.isInsideVM = function() {
          return Luser.isInsideVM();
        }
      },
      constrollerAs: 'vm'
    })
    .state('linux.vm', {
      abstract: true,
      url: '/withinVM',
      template: '<div ui-view></div>'
    })
    .state('linuxShortcut', {
      url: '/tud/linux',
      template: '',
      controller: function($state) {
        $state.go('linux.home');
      }
    })
};
