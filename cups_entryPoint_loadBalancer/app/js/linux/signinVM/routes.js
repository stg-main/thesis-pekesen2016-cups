'use strict';

//@ngInject
module.exports = function routes($stateProvider) {

  $stateProvider
    .state('linux.vm.signin', {
      url: '/signin',
      template: require('../signinVM/signinVM.html'),
      controller: 'LSigninVMController',
      controllerAs: 'vm',
      bindToController: true
    })
};
