'use strict';

var component = require('./signinVM.module');

class SignupController {
  constructor(LauthResource, Luser, $state, $alert) {
    if (Luser.loggedIn()) {
      $state.go('linux.vm.study');
    }
    this.$state = $state;
    this.user = Luser;
    this.auth = LauthResource;
    this.$alert = $alert;
  }

  signin() {
    if (!this.user.loggedIn()) {
      if (_.get(this, 'email.length', '') > 0 && _.get(this, 'password.length', '') > 0) {
        this.auth.signInVM(this.email, this.password)
          .then(() => {
            this.$state.go('linux.vm.study');
          });
      }
    }}
}

component.controller('LSigninVMController', SignupController);


