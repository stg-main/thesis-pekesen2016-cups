'use strict';

var routes = require('./routes');
var requires = [];

module.exports = angular.module('app.linux.signinVM', requires)
  .config(routes);

// controllers
require('./signinVM.controller');

// directives

// services
