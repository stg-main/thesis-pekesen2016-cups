'use strict';

//@ngInject
module.exports = function routes($stateProvider) {

  $stateProvider
    .state('linux.signup', {
      url: '/signup',
      template: require('../signup/signup.html'),
      controller: 'LSignupController',
      controllerAs: 'vm',
      bindToController: true
    })
};
