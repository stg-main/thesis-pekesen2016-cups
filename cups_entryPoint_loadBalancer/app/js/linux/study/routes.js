'use strict';

//@ngInject
module.exports = function routes($stateProvider) {

  $stateProvider
    .state('linux.vm.study', {
      url: '/study',
      template: require('../study/study.html'),
      controller: 'LStudyController',
      controllerAs: 'vm',
      bindToController: true
    })
};
