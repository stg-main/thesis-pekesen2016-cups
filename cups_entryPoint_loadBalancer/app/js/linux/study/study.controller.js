'use strict';

var component = require('./study.module');

class StudyController {
  constructor(Luser, $state, LparticipantResource) {
    this.participantResource = LparticipantResource;
    this.$state = $state;

    if (!Luser.loggedIn()) {
      $state.go('linux.home');
    }
    if (Luser.finishedStudy()) {
      $state.go('linux.vm.feedback');
    }
  }
  
  next() {
    this.participantResource.endStudy()
      .then(() => {
        this.$state.go('linux.vm.feedback');
      });    
  }
}

component.controller('LStudyController', StudyController);
