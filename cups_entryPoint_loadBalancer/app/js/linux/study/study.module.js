'use strict';

var routes = require('./routes');
var requires = [];

module.exports = angular.module('app.linux.study', requires)
  .config(routes);

// controllers
require('./study.controller');

// directives

// services
