'use strict';

//@ngInject
module.exports = function routes($stateProvider) {

  $stateProvider
    .state('linux.studyintro', {
      url: '/studyintro',
      template: require('../studyintro/studyintro.html'),
      controller: 'LStudyIntroController',
      controllerAs: 'vm',
      bindToController: true
    })
};
