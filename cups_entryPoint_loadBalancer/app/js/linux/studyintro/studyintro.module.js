'use strict';

var routes = require('./routes');
var requires = [];

module.exports = angular.module('app.linux.studyintro', requires)
  .config(routes);

// controllers
require('./studyintro.controller');

// directives

// services
