'use strict';

//@ngInject
module.exports = function routes($stateProvider) {

  $stateProvider
    .state('linux.thankyou', {
      url: '/thankyou',
      template: require('../thankyou/thankyou.html')
    })
};
