'use strict';

//@if !isProd
window.LiveReloadOptions = {host: 'localhost'};
require('livereload-js');
//@endif

// Angular
require('angular');
require('angular-ui-router');
require('angular-resource');
require('angular-strap');
require('angular-spinner');

require('./templatecache');
require('./translation');
require('./windows');
require('./linux');

// linux app

// create and bootstrap application

var requires = [
  'ngResource',
  'angularSpinner',
  'mgcrea.ngStrap',
  'app.translation',
  'ui.router',
  'app.windows',
  'app.linux'
];

module.exports = angular.module('app', requires)
  .config(($locationProvider, $urlRouterProvider) => {
    $locationProvider.html5Mode(true);
    //windows as default
    //$urlRouterProvider.otherwise("/Wv7YxkSf4/home");
    //linux as default
    $urlRouterProvider.otherwise("/LkUg7u2MS/home");
  });

// // alle shortcuts abfangen bis auf shift + tab
// window.document.onkeydown = handleKeyDown(true);
// window.document.onkeypress = handleKeyDown(false);
// window.document.onkeyup = handleKeyDown(false);
function handleKeyDown(isLogging) {

  return function(e) {

    var ctrlPressed=0;
    var altPressed=0;
    var shiftPressed=0;

    var evt = (e==null ? event:e);

    shiftPressed=evt.shiftKey;
    altPressed  =evt.altKey;
    ctrlPressed =evt.ctrlKey;
    self.status=""
      +  "shiftKey="+shiftPressed
      +", altKey="  +altPressed
      +", ctrlKey=" +ctrlPressed;

    if ((shiftPressed || altPressed || ctrlPressed)
      && (evt.keyCode < 16 || evt.keyCode > 18) && evt.keyCode !== 9){
      evt.preventDefault();
      evt.stopPropagation();

      if (isLogging) {
        console.log ("You pressed the "+String.fromCharCode(evt.keyCode)
          +" key (keyCode "+evt.keyCode+")\n"
          +"together with the following keys:\n"
          + (shiftPressed ? "Shift ":"")
          + (altPressed   ? "Alt "  :"")
          + (ctrlPressed  ? "Ctrl " :"")
        );
      }
    }
    return true;
  };
}
