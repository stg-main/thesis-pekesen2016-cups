'use strict';

var routes = require('./routes');
var requires = [];

module.exports = angular.module('app.windows.about', requires)
  .config(routes);

// controllers

// directives

// services
