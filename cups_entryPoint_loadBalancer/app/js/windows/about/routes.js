'use strict';

//@ngInject
module.exports = function routes($stateProvider) {

  $stateProvider
    .state('windows.about', {
      url: '/about',
      template: require('../about/about.html')
    })
};
