'use strict';

var component = require('./api.module');

component.service('participantResource', participantResource);

function participantResource($resource, user) {
  var Participant = $resource('/api/v1/participant/vm');
  var EndStudy = $resource('/api/v1/participant/end');
  var Feedback = $resource('/api/v1/participant/feedback');
  var CancelStudy = $resource('/api/v1/participant/cancel');
  var Interactions = $resource('/api/v1/participant/focus');
  var Client = $resource('/api/v1/participant/client');

  return {
    getVM: getVM,
    endStudy: endStudy,
    submitFeedback: submitFeedback,
    cancelStudy: cancelStudy,
    sendInteractions: sendInteractions,
    sendClientInfo: sendClientInfo
  };

  function getVM() {
    return Participant.get().$promise
      .then((res) => {
        user.loadState();
        return res;
      });
  }

  function endStudy() {
    return EndStudy.save().$promise
      .then((res) => {
        return user.loadState()
          .then(() => {
            return res
          });
      });
  }

  function cancelStudy(cancelInfo) {
    return CancelStudy.save({cancelInfo: cancelInfo}).$promise
      .then((res) => {
        return user.loadState()
          .then(() => {
            return res
          });
      });
  }

  function submitFeedback(feedback) {
    return Feedback.save({feedback: feedback}).$promise
      .then((res) => {
        return user.loadState()
          .then(() => {
            return res
          });
      });
  }

  function sendInteractions() {
    return Interactions.save({interactions: user.introWindowInteractions}).$promise
      .then(() => {
        return user.resetInteractions();
      });
  }

  function sendClientInfo(clientInfo) {
    return Client.save({clientInfo: clientInfo}).$promise;
  }
}

