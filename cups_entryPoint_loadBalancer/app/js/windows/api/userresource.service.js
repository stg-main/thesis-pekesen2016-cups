'use strict';

var component = require('./api.module');

component.service('userResource', authResourceService);

function authResourceService($resource) {
  var State = $resource('/api/v1/users/state');

  return {
    getState: getState
  };

  function getState() {
    return State.get().$promise;
  }
}

