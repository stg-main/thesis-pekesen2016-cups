'use strict';

var requires = [
  'app.windows.api'
];

module.exports = angular.module('app.windows.auth', requires);

// controllers

// directives

// services
require('./crypt.service');
require('./user.service');

// routes
