'use strict';

var _ = require('lodash');

var component = require('./auth.module');

class userService {
  constructor(userResource, crypt, $state) {
    this.crypt = crypt;
    this.user = this.getLocal();

    this.auth = userResource;
    this.state = $state;

    this.introWindowInteractions = {
      focusGain: [],
      focusLost: []
    };

    this.loadState();

  }

  loadState() {
    return this.auth.getState().then((user) => {
      this.user = user;
      this.setLocal(this.user);
    })
      .catch(() => {
        this.user = {
          email: '',
          id: '',
          vm: {},
          linux: {
            vm: {}
          }
        };
      });
  }

  resetInteractions() {
    this.introWindowInteractions = {
      focusGain: [],
      focusLost: []
    };
  }
  
  isClientInfoAvailable() {
    return _.get(this.user, 'clientInfo', false) ? true : false;
  }

  isCancelled() {
    return _.get(this.user, 'cancelled', false);
  }

  isInsideVM() {
    return this.state.includes('windows.vm');
  }

  loggedIn() {
    return this.user.email.length > 0;
  }

  finishedStudy() {
    return _.get(this.user, 'finishedStudy', false);
  }

  finishedFeedback() {
    return _.get(this.user, 'finishedFeedback', false);
  }

  getLocal() {
    var user = window.localStorage.getItem('cups-user');
    if (user) {
      return this.crypt.decrypt(user);
    } else {
      return {
        email: '',
        id: ''
      };
    }
  }

  clearLocal() {
    window.localStorage.removeItem('cups-user');
  }

  setLocal(user) {
    window.localStorage.setItem('cups-user', this.crypt.encrypt(user));
  }

}

component.service('user', userService);
