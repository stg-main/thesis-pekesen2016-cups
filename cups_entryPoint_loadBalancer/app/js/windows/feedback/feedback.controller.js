'use strict';

var components = require('./feedback.module');

var participant, state;

class FeedbackController {
  constructor($state, user, participantResource) {
    if (!user.loggedIn()) {
      $state.go('windows.home');
    }
    if (!user.finishedStudy()) {
      window.alert('You have to finish the evaluation first before giving feedback!');
      $state.go('windows.studyintro');
    }
    if (user.finishedFeedback()) {
      $state.go('windows.thankyou');
    }

    participant = participantResource;
    state = $state;
  }

  submit() {
    participant.submitFeedback(this.feedback)
      .then(() => {
        state.go('windows.thankyou');
      });
  }
}

components.controller('FeedbackController', FeedbackController);
