'use strict';

var routes = require('./routes');
var requires = [];

module.exports = angular.module('app.windows.feedback', requires)
  .config(routes);

// controllers
require('./feedback.controller');

// directives

// services
