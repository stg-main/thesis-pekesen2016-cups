'use strict';

//@ngInject
module.exports = function routes($stateProvider) {

  $stateProvider
    .state('windows.vm.feedback', {
      url: '/feedback',
      template: require('../feedback/feedback.html'),
      controller: 'FeedbackController',
      controllerAs: 'vm',
      bindToController: true
    })
};
