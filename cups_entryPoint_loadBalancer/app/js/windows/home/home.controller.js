'use strict';

var component = require('./home.module');

class HomeController {
  constructor($state, user) {
    this.$state = $state;
    this.user = user;
  }
  
  go(state) {
    this.$state.go(state);
  }
}

component.controller('HomeController', HomeController);


