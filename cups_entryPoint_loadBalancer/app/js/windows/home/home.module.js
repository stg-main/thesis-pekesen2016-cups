'use strict';

var routes = require('./routes');
var requires = [];

module.exports = angular.module('app.windows.home', requires)
  .config(routes);

// controllers
require('./home.controller');

// directives

// services
