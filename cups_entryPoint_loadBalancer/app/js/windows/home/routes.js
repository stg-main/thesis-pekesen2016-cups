'use strict';

//@ngInject
module.exports = function routes($stateProvider) {

  $stateProvider
    .state('windows.home', {
      url: '/home',
      template: require('../home/home.html'),
      controller: 'HomeController',
      controllerAs: 'vm',
      bindToController: true
    })
};
