var component = require('./navigation.module');
var _ = require('lodash');

class NavigationController {
  constructor(authResource, user, $state) {
    this.user = user;
    this.auth = authResource;
    this.$state = $state;
  }
  
  isActive(stateName) {
    return this.$state.is(stateName) ? 'active' : '';
  }

  login() {
    if (!this.user.loggedIn()) {
      if (_.get(this, 'email.length', '') > 0 && _.get(this, 'password.length', '') > 0) {
        this.auth.signIn(this.email, this.password)
          .then(() => {
            this.email = '';
            this.password = '';
            
            this.$state.go('windows.studyintro');
          });
      }
    }
  }

  logout() {
    if (this.user.loggedIn()) {
      this.auth.logOut();
    }
  }
}

component.controller('NavigationController', NavigationController);
