'use sctrict';

var component = require('./navigation.module');

component.directive('navigation', navigation);

function navigation() {
  return {
    restrict: 'E',
    template: require('./navigation.cache.html'),
    controller: 'NavigationController',
    controllerAs: 'vm'
  }
}
