var requires = [
  'app.windows.api'
];

module.exports = angular.module('app.windows.navigation', requires);


// controllers
require('./navigation.controller');

// directives
require('./navigation.directive');

// services
