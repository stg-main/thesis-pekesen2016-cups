'use strict';

//@ngInject
module.exports = function routes($stateProvider) {

  $stateProvider
    .state('windows', {
      abstract: true,
      url: '/Wv7YxkSf4',
      template: '<navigation ng-hide="isInsideVM()"></navigation> <div class="content col-md-8 col-sm-12" ui-view></div> <img class="pull-right logo col-md-3 hidden-sm hidden-xs" ng-src="dist/images/cupsLogo.png">',
      controller: function($scope, user) {
        $scope.isInsideVM = function() {
          return user.isInsideVM();
        }
      },
      constrollerAs: 'vm'
    })
    .state('windows.vm', {
      abstract: true,
      url: '/withinVM',
      template: '<div ui-view></div>'
    })
    .state('windowsShortcut', {
      url: '/tud/windows',
      template: '',
      controller: function($state) {
        $state.go('windows.home');
      }
    })
};
