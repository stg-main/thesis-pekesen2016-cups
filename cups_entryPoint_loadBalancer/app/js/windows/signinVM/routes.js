'use strict';

//@ngInject
module.exports = function routes($stateProvider) {

  $stateProvider
    .state('windows.vm.signin', {
      url: '/signin',
      template: require('../signinVM/signinVM.html'),
      controller: 'SigninVMController',
      controllerAs: 'vm',
      bindToController: true
    })
};
