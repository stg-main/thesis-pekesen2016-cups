'use strict';

var component = require('./signinVM.module');

class SignupController {
  constructor(authResource, user, $state, $alert) {
    if (user.loggedIn()) {
      $state.go('windows.vm.study');
    }
    this.$state = $state;
    this.user = user;
    this.auth = authResource;
    this.$alert = $alert;
  }

  signin() {
    if (!this.user.loggedIn()) {
      if (_.get(this, 'email.length', '') > 0 && _.get(this, 'password.length', '') > 0) {
        this.auth.signInVM(this.email, this.password)
          .then(() => {
            this.$state.go('windows.vm.study');
          })
          .catch(() => {
            window.alert('Something went wrong! Please try again.');
          });
      }
    }}
}

component.controller('SigninVMController', SignupController);


