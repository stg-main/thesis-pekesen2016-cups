'use strict';

//@ngInject
module.exports = function routes($stateProvider) {

  $stateProvider
    .state('windows.signup', {
      url: '/signup',
      template: require('../signup/signup.html'),
      controller: 'SignupController',
      controllerAs: 'vm',
      bindToController: true
    })
};
