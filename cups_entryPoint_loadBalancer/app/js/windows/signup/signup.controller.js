'use strict';

var component = require('./signup.module');

class SignupController {
  constructor(authResource, user, $state, $alert) {
    if (user.loggedIn()) {
      $state.go('windows.home');
    }
    this.$state = $state;
    this.user = user;
    this.auth = authResource;
    this.$alert = $alert;
  }

  signup() {
    if (!this.user.loggedIn()) {
      if (_.get(this, 'email.length', '') > 0 && _.get(this, 'password.length', '') > 0) {
        this.auth.signUp(this.email, this.password)
          .then(() => {
            this.$state.go('windows.studyintro');
          })
          .catch((res) => {
            if (res.status === 409) {
              window.alert('Username: ' + this.email + ' already in use. Please pick another one!');
            } else {
              window.alert('Something went wrong! Please try again.');
            }
          });
      }
    }}
}

component.controller('SignupController', SignupController);


