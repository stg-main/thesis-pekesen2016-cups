'use strict';

var routes = require('./routes');
var requires = [];

module.exports = angular.module('app.windows.signup', requires)
  .config(routes);

// controllers
require('./signup.controller');

// directives

// services
