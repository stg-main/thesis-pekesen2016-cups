'use strict';

//@ngInject
module.exports = function routes($stateProvider) {

  $stateProvider
    .state('windows.vm.study', {
      url: '/study',
      template: require('../study/study.html'),
      controller: 'StudyController',
      controllerAs: 'vm',
      bindToController: true
    })
};
