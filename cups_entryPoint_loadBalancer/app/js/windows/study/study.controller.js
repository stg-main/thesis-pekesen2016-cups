'use strict';

var component = require('./study.module');

class StudyController {
  constructor(user, $state, participantResource) {
    this.participantResource = participantResource;
    this.state = $state;

    if (!user.loggedIn()) {
      $state.go('windows.home');
    }
    if (user.finishedStudy()) {
      $state.go('windows.vm.feedback');
    }

    this.buttonCount = 0;
    this.buttonText = 'End Evaluation';
  }

  endStudy() {
    if (this.buttonCount < 1) {
      this.buttonCount++;
      this.buttonText = 'Are you Sure? This can not be undone!';
    } else {
      this.participantResource.endStudy().
      then(() => {
        this.state.go('windows.vm.feedback');
      });
    }
  }

  resetCount() {
    this.buttonText = 'End Evaluation';
    this.buttonCount = 0;
  }
}

component.controller('StudyController', StudyController);
