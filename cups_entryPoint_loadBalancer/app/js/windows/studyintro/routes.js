'use strict';

//@ngInject
module.exports = function routes($stateProvider) {

  $stateProvider
    .state('windows.studyintro', {
      url: '/studyintro',
      template: require('../studyintro/studyintro.html'),
      controller: 'StudyIntroController',
      controllerAs: 'vm',
      bindToController: true
    })
};
