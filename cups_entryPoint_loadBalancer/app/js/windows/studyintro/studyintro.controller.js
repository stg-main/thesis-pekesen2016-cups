'use strict';

var component = require('./studyintro.module');
var ifvisible = require('ifvisible.js');
require('browser-report');
var _ = require('lodash');

class StudyController {
  constructor(user, $state, participantResource, $interval, $scope, $timeout) {
    this.participantResource = participantResource;
    this.state = $state;
    this.user = user;
    this.interval = $interval;
    this.timeout = $timeout;

    if (!user.isClientInfoAvailable()) {
      browserReport((err, report) => {
        if (err) {
          console.error(err);
        } else {
          this.participantResource.sendClientInfo(report);
        }
      });
    }

    if (!user.loggedIn()) {
      this.endRecording(true);
      return $state.go('windows.home');
    }
    if (user.finishedStudy()) {
      this.endRecording(true);
      return $state.go('windows.vm.feedback');
    }

    this.vm = user.user.vm;

    this.buttonCount = 0;
    this.buttonText = 'Cancel';

    this.getVM();
    this.stopInterval = $interval(this._checkProgress.bind(this), 10000);

    ifvisible.setIdleDuration(10);
    this.startRecording();

    $scope.$on('$destroy', () => {
      ifvisible.off("idle");
      ifvisible.off("wakeup");
      this.user.resetInteractions();
      this.interval.cancel(this.stopInterval);
      if (this.timer) {
        this.timeout.cancel(this.timer);
      }
    });
  }

  _checkProgress() {
    this.user.loadState()
      .then(() => {
        if (this.user.finishedStudy() && !this.user.isCancelled()) {
          this.user.loadState()
            .then(() => {
              this.endRecording();
              return this.state.go('windows.thankyou');
            })
            .catch(() => {
              this.endRecording();
              return this.state.go('windows.thankyou');
            });
        }

        // cancel the loop if study was cancelled
        if (this.user.isCancelled()) {
          return this.interval.cancel(this.stopInterval);
        }
      });

  }

  endRecording(dontSendToServer) {
    ifvisible.off("idle");
    ifvisible.off("wakeup");
    if (!dontSendToServer) {
      this.participantResource.sendInteractions();
    }
    this.user.resetInteractions();
  }

  startRecording() {
    ifvisible.on("idle", () => {
      this.user.introWindowInteractions.focusLost.push(new Date());
    });

    ifvisible.on("wakeup", () => {
      this.user.introWindowInteractions.focusGain.push(new Date());
    });
  }

  getVM() {
    this.spinnerActive = true;

    this.participantResource.getVM()
      .then((response) => {
        if (response.status === 'pending') {
          this.timer = this.timeout(this.getVM.bind(this), 10000);
        } else {
          this.vm = response;
          this.spinnerActive = false;
        }
      });
  }

  cancelStudy() {
    if (this.buttonCount < 1) {
      this.buttonCount++;
      this.buttonText = 'Are you Sure? This can not be undone!';
    } else {
      this.participantResource.cancelStudy(this.cancelInfo).
        then(() => {
        this.endRecording();
        this.state.go('windows.thankyou');
      });
    }
  }

  resetCount() {
    this.buttonText = 'Cancel';
    this.buttonCount = 0;
  }

  cancelable() {
    var cause = _.get(this, 'cancelInfo.cause', false);
    if (cause) {
      if (cause === 'others') {
        var details = _.get(this, 'cancelInfo.details', '');
        return details.length > 0;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }
}

component.controller('StudyIntroController', StudyController);
