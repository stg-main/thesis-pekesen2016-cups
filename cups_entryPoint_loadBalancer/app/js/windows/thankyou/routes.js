'use strict';

//@ngInject
module.exports = function routes($stateProvider) {

  $stateProvider
    .state('windows.thankyou', {
      url: '/thankyou',
      template: require('../thankyou/thankyou.html')
    })
};
