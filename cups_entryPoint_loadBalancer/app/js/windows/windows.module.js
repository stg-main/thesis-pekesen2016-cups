'use strict';

var routes = require('./routes');

require('./api');
require('./auth');
require('./signup');
require('./signinVM');
require('./about');
require('./study');
require('./studyintro');
require('./feedback');
require('./thankyou');
require('./home');
require('./navigation');

var requires = [
  'app.windows.home',
  'app.windows.api',
  'app.windows.auth',
  'app.windows.about',
  'app.windows.signup',
  'app.windows.signinVM',
  'app.windows.studyintro',
  'app.windows.study',
  'app.windows.feedback',
  'app.windows.thankyou',
  'app.windows.navigation'
];

module.exports = angular.module('app.windows', requires)
  .config(routes);
