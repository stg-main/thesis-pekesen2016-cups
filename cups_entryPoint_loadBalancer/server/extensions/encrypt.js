var pubBackKey = '-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAqLAyY+xl4G5LB0J+5+AYoBEMWlYXJpa0nKLO/qCqEJAKjOeomOR0kv8XFjWu\nezkXlfljm9ecLQ1rJpEmThYH5MHEW9AYXZlgQoNPyCMqVWBLBLLGHqu/12H/RRsGxbb0ulP4\nKXldz7lhlOWdVah99XMADae9rq8bye+u/m8rLcNqDE6hHwTDJ91xjxLltqG9ANLE0SnBuUc7\n1ndOXthpn6CoQTalgeW15suCj2T80pk8z0xn/BAR1Hf0DyVg3xL6gb0M8baqDB9qITUAWXjM\n7WbZSEaotfwisxw1Zf0Dnk2GEwzq9FHfvh8a1qXbBWTy2Fb9wXTVlQ4ZKXqhfDMA6QIDAQAB\n-----END RSA PUBLIC KEY-----\n';

module.exports = Encrypt;

Encrypt.$inject = ['NodeRSA', 'bodySecret'];
function Encrypt(NodeRSA, bodySecret) {
  var encryptObject = NodeRSA(pubBackKey);

  function encrypt(data) {
    data.secret = bodySecret;
    var msg = {
      data: data
    };
    msg.data = encryptObject.encrypt(msg.data, 'base64');

    return msg;
  }

  return encrypt;
}
