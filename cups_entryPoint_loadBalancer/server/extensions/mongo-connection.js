module.exports = MongoConnection;

MongoConnection.$inject = ['logFunc', 'mongoose', 'mongoUri', 'mongooseAutoIncrement'];
// initial mongo setup
function MongoConnection(log, mongoose, mongoUri, mongooseAutoIncrement) {
  // connect to userDatabase
  var connection = mongoose.connect(mongoUri, function (err) {
    if (err) log(err);
  });


  mongooseAutoIncrement.initialize(connection);

  return connection;
}
