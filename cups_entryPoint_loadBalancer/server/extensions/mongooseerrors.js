module.exports = MongooseErrors;

MongooseErrors.$inject = [];
function MongooseErrors() {
  return {
    notFound: 'NOT_FOUND',
    save: 'MONGOOSE_SAVE_ERROR',
    find: 'MONGOOSE_FIND_ERROR',
    remove: 'MONGOOSE_REMOVE_ERROR',
    conflict: 'MONGOOSE_RESOURCE_CONFLICT',
    notAllowed: 'NOT_ALLOWED_ATTRIBUTES',
    tooEarly: 'TOO_EARLY',
    expired: 'EXPIRED',
    populate: 'MONGOOSE_POPULATE_ERROR',
    notEnough: 'NOT_ENOUGH_RESOURCES'
  };
}