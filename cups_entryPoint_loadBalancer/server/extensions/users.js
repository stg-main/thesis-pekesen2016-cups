var bcrypt = require('bcrypt');
var _ = require('lodash');
var SALT_WORK_FACTOR;
var MAX_LOGIN_ATTEMPTS;
var LOCK_TIME;

module.exports = UserModel;

UserModel.$inject = ['config', 'q', 'mongoose', 'mongooseErrors', 'mongooseAutoIncrement'];
function UserModel(config, q, mongoose, errors, mongooseAutoIncrement) {
  SALT_WORK_FACTOR = config.userModel.salt_work_factor;
  MAX_LOGIN_ATTEMPTS = config.userModel.max_login_attempts;
  LOCK_TIME = config.userModel.lock_time_10_min;

  var Schema = mongoose.Schema;

  var UserSchema = new Schema({

    // general attributes
      email: {type: String, required: true, index: {unique: true}},
      password: String,
      number: Number,
      loginAttempts: {type: Number, required: true, default: 0},
      lockUntil: {type: Number},
      isLinux:  {type: Boolean, default: false},
      isWindows:  {type: Boolean, default: false},

    // windows specific attributes
      finishedStudy: {type: Boolean, default: false},
      finishedFeedback: {type: Boolean, default: false},
      feedback: Schema.Types.Mixed,
      cancelled: {type: Boolean, default: false},
      cancelledDate: Date,
      cancelInfo: Schema.Types.Mixed,
      signedInVMDate: Date,
      rdpDisplayedDate: Date,
      finishedStudyDate: Date,
      finishedFeedbackDate: Date,
      introInteractionDates: {
        focusGain: {type: [Date], default: []},
        focusLost: {type: [Date], default: []}
      },
      clientInfo: Schema.Types.Mixed,

      vm: {
        username: String,
        password: String,
        pending: {type: Boolean, default: false}
      },

    // linux specific attributes
      linux: {
        finishedStudy: {type: Boolean, default: false},
        finishedFeedback: {type: Boolean, default: false},
        feedback: Schema.Types.Mixed,
        cancelled: {type: Boolean, default: false},
        cancelledDate: Date,
        cancelInfo: Schema.Types.Mixed,
        signedInVMDate: Date,
        rdpDisplayedDate: Date,
        finishedStudyDate: Date,
        finishedFeedbackDate: Date,
        introInteractionDates: {
          focusGain: {type: [Date], default: []},
          focusLost: {type: [Date], default: []}
        },
        clientInfo: Schema.Types.Mixed,

        vm: {
          username: String,
          password: String,
          pending: {type: Boolean, default: false}
        }
      }

    },
    {collection: 'users'});

  UserSchema.plugin(mongooseAutoIncrement.plugin, {
    model: 'users',
    field: 'number',
    startAt: 1,
    incrementBy: 1
  });


  UserSchema.virtual('isLocked').get(function () {
    // check for a future lockUntil timestamp
    return !!(this.lockUntil && this.lockUntil > Date.now());
  });

  UserSchema.pre('save', function (next) {
    var user = this;
    // break if no password is set
    if (!user.password) {
      return next();
    }

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) {
      return next();
    }

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
      if (err) return next(err);

      // hash the password using our new salt
      bcrypt.hash(user.password, salt, function (err, hash) {
        if (err) return next(err);

        // set the hashed password back on our user document
        user.password = hash;
        return next();
      });
    });
  });

  UserSchema.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
      if (err) return cb(err);
      cb(null, isMatch);
    });
  };

  UserSchema.methods.incLoginAttempts = function (cb) {
    // if we have a previous lock that has expired, restart at 1
    if (this.lockUntil && this.lockUntil < Date.now()) {
      return this.update({
        $set: {loginAttempts: 1},
        $unset: {lockUntil: 1}
      }, cb);
    }
    // otherwise we're incrementing
    var updates = {$inc: {loginAttempts: 1}};
    // lock the account if we've reached max attempts and it's not locked already
    if (this.loginAttempts + 1 >= MAX_LOGIN_ATTEMPTS && !this.isLocked) {
      updates.$set = {lockUntil: Date.now() + LOCK_TIME};
    }
    return this.update(updates, cb);
  };

// expose enum on the model, and provide an internal convenience reference
  var reasons = UserSchema.statics.failedLogin = {
    NOT_FOUND: 'NOT_FOUND',
    PASSWORD_INCORRECT: 'PASSWORD_INCORRECT',
    MAX_ATTEMPTS: 'MAX_ATTEMPTS',
    DUPLICATE_EMAIL: 'DUPLICATE_EMAIL',
    ALREADY_LOGGED_IN: 'ALREADY_LOGGED_IN'
  };

  UserSchema.statics.getAuthenticated = function (email, password, cb) {
    this.findOne({'email': email}, function (err, user) {
      if (err) return cb(err);

      // make sure the user exists
      if (!user) {
        return cb(null, false, reasons.NOT_FOUND);
      }

      // check if the account is currently locked
      if (user.isLocked) {
        // just increment login attempts if account is already locked
        return user.incLoginAttempts(function (err) {
          if (err) return cb(err);
          return cb(null, false, reasons.MAX_ATTEMPTS);
        });
      }

      // test for a matching password
      user.comparePassword(password, function (err, isMatch) {
        if (err) return cb(err);

        // check if the password was a match
        if (isMatch) {
          // if there's no lock or failed attempts and the password does not
          // need to be re-hashed, just return the user
          if (!user.loginAttempts && !user.lockUntil && bcrypt.getRounds(user.password) === SALT_WORK_FACTOR) {
            return cb(null, user);
          }
          // reset attempts and lock info
          var updates = {
            $set: {loginAttempts: 0},
            $unset: {lockUntil: 1}
          };

          /* TODO ASYNC PROBLEM
           // check if re-hashing the password is necessary and if so, do it
           if (bcrypt.getRounds(user.password) !== SALT_WORK_FACTOR) {
           // generate a salt
           bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
           if (err) return next(err);

           // hash the password using our new salt
           bcrypt.hash(password, salt, function (err, hash) {
           if (err) return next(err);

           // set the hashed password to our updates object
           updates.$set.password = hash;
           return user.update(updates, function(err) {
           if (err) return cb(err);
           return cb(null, user);
           });
           });
           });
           } else {
           return user.update(updates, function(err) {
           if (err) return cb(err);
           return cb(null, user);
           });
           }
           */

          // Sync solution, not prefered!
          // check if re-hashing the password is necessary and if so, do it
          if (bcrypt.getRounds(user.password) !== SALT_WORK_FACTOR) {
            // hash the password using our new salt
            // set the hashed password to our updates object
            updates.$set['password'] = bcrypt.hashSync(password, SALT_WORK_FACTOR);
          }

          return user.update(updates, function (err) {
            if (err) return cb(err);
            return cb(null, user);
          });

        }

        // password is incorrect, so increment login attempts before responding
        user.incLoginAttempts(function (err) {
          if (err) return cb(err);
          return cb(null, false, reasons.PASSWORD_INCORRECT);
        });
      });
    });
  };

  /////////////////////
  //// API METHODS ////
  /////////////////////

  UserSchema.statics.get = function(email) {
    var def = q.defer();

    this.findOne({email: email}, function(err, user) {
      if (err) {
        return def.reject(errors.find);
      }

      if (!user) {
        return def.reject(errors.notFound);
      } else {
        return def.resolve(user);
      }
    });

    return def.promise;
  };

  UserSchema.statics.getById = function(id) {
    var def = q.defer();

    this.findById(id, function(err, user) {
      if (err) {
        return def.reject(errors.find);
      }

      if (!user) {
        return def.reject(errors.notFound);
      } else {
        return def.resolve(user);
      }
    });

    return def.promise;
  };

  UserSchema.statics.edit = function(email, attrs) {
    var def = q.defer();

    var self = this;

    this.findOne({email: email}, function(err, user) {
      if (err) {
        return def.reject(errors.find);
      }

      if (!user) {
        return def.reject(errors.notFound);
      } else {
        Object.keys(attrs).forEach(function(attr) {
          user[attr] = attrs[attr];
        });

        user.save(function(err) {
          if (err) {
            return def.reject({error: errors.save})
          }

          return def.resolve(self);
        });
      }
    });


    return def.promise;
  };

  UserSchema.methods.editVM = function(attrs, isLinux) {
    var def = q.defer();

    var self = this;

    // log if it user is linux eval participant
    if (isLinux) {
      this.isLinux = true;
    }

    // log if it user is windows eval participant
    if (!isLinux) {
      this.isWindows = true;
    }

    Object.keys(attrs).forEach(function(attr) {
      if (isLinux) {
        self.linux.vm[attr] = attrs[attr];
      } else {
        self.vm[attr] = attrs[attr];
      }
    });

    // log when data arrived
    if (attrs.username && attrs.password) {
      if (isLinux) {
        this.linux.rdpDisplayedDate = new Date();
      } else {
        this.rdpDisplayedDate = new Date();
      }
    }

    self.save(function(err) {
      if (err) {
        return def.reject({error: errors.save})
      }

      return def.resolve(self);
    });


    return def.promise;
  };

  UserSchema.methods.finishStudy = function(isLinux) {
    var def = q.defer();

    var self = this;

    if (isLinux) {
      this.linux.finishedStudy = true;
      this.linux.finishedStudyDate = new Date();
    } else {
      this.finishedStudy = true;
      this.finishedStudyDate = new Date();
    }
    this.save(function(err) {
      if (err) {
        return def.reject(err);
      }
      return def.resolve(self);
    });

    return def.promise;
  };

  UserSchema.methods.submitFeedback = function(feedback, isLinux) {
    var def = q.defer();
    var self = this;

    if (isLinux) {
      this.linux.feedback = feedback;
      this.linux.finishedFeedback = true;
      this.linux.finishedFeedbackDate = new Date();
    } else {
      this.feedback = feedback;
      this.finishedFeedback = true;
      this.finishedFeedbackDate = new Date();
    }

    this.save(function(err) {
      if (err) {
        return def.reject(err);
      }
      return def.resolve(self);
    });

    return def.promise;
  };

  UserSchema.methods.cancel = function(cancelInfo, isLinux) {
    var def = q.defer();
    var self = this;

    if (isLinux) {
      this.linux.cancelled = true;
      this.linux.finishedStudy = true;
      this.linux.cancelledDate = new Date();
      this.linux.cancelInfo = cancelInfo;
    } else {
      this.cancelled = true;
      this.finishedStudy = true;
      this.cancelledDate = new Date();
      this.cancelInfo = cancelInfo;
    }

    this.save(function(err) {
      if (err) {
        return def.reject(err);
      }
      return def.resolve(self);
    });

    return def.promise;
  };

  UserSchema.methods.signInVM = function(isLinux) {
    var def = q.defer();
    var self = this;

    if (isLinux) {
      this.linux.signedInVMDate = new Date();
    } else {
      this.signedInVMDate = new Date();
    }

    this.save(function(err) {
      if (err) {
        return def.reject(err);
      }
      return def.resolve(self);
    });

    return def.promise;
  };

  UserSchema.methods.setInteractions = function(interactions, isLinux) {
    var def = q.defer();
    var self = this;

    if (isLinux) {
      this.linux.introInteractionDates = interactions;
    } else {
      this.introInteractionDates = interactions;
    }

    this.save(function(err) {
      if (err) {
        return def.reject(err);
      }
      return def.resolve(self);
    });

    return def.promise;
  };

  UserSchema.methods.setClientInfo = function(info, isLinux) {
    var def = q.defer();
    var self = this;

    if (isLinux) {
      this.linux.clientInfo = info;
    } else {
      this.clientInfo = info;
    }

    this.save(function(err) {
      if (err) {
        return def.reject(err);
      }
      return def.resolve(self);
    });

    return def.promise;
  };




  return mongoose.model('users', UserSchema);
}
