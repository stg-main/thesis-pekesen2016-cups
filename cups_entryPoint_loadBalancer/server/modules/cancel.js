'use strict';

var _ = require('lodash');

module.exports = Cancel;

Cancel.$inject = ['apiv1', 'users', 'request', 'encrypt', 'config'];
function Cancel(server, User, request, encrypt, config) {
  server.post('/participant/cancel', function(req, res) {

    if (!req.user) {
      return res.sendStatus(403);
    }

    if (req.user.connectionFailed) {
      res.sendStatus(200);

    } else {
      var username;
      var user;
      
      // user holen
      User.getById(req.user.id)
        .then(function (resolvedUser) {
          user = resolvedUser;
          username = user.vm.username;
          
          return resolvedUser.cancel(req.body.cancelInfo);
        })
        .then(function() {
          console.log('Participant\'s connection failed: ' + req.user.email);
          res.sendStatus(200);

          var msg = {username: username};
          msg = encrypt(msg);

          request({
            url: config.backend.host + '/user/end',
            body: msg,
            json: true
          }, function (error, response, body) {
            if (error) {
              console.error('Something went wrong in the request: ', error);
            }

            console.log('Participant \'' + req.user.email + '\' has ended its evaluation!');
            console.log('VM User \'' + username + '\' has been logged off and its password got resetted.');
          });
        })
        .catch(function (e) {
          console.log(e);
          res.sendStatus(500)
        });
    }

  });
}
