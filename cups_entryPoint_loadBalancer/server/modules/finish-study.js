'use strict';

var _ = require('lodash');

module.exports = CreateUser;

CreateUser.$inject = ['apiv1', 'users'];
function CreateUser(server, User) {
  server.post('/participant/end', function(req, res) {

    if (!req.user) {
      return res.sendStatus(403);
    }

    if (req.user.finishedStudy) {
      res.sendStatus(200);

    } else {

      var username;
      var user;

      // user holen
      User.getById(req.user.id)
        .then(function (resolvedUser) {
          user = resolvedUser;
          username = user.vm.username;

          // flag "finishedStudy setzen
          user.finishStudy()
            .then(function() {
              res.sendStatus(200);
            });
        })
        .catch(function (e) {
          console.log(e);
          res.sendStatus(500)
        });
    }

  });
}
