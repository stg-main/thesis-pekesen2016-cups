'use strict';

module.exports = Focus;

Focus.$inject = ['apiv1'];
function Focus(server) {
  server.post('/participant/focus', function(req, res) {

    if (!req.user) {
      return res.sendStatus(403);
    }

    req.user.setInteractions(req.body.interactions)
      .then(function() {
        res.sendStatus(200);
      })
      .catch(function() {
        res.sendStatus(500);
      })

  });
}
