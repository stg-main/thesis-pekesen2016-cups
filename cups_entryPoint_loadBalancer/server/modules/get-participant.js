'use strict';

var Moniker = require('moniker');
var moniker = Moniker.generator([Moniker.adjective, Moniker.noun], {"maxSize":8});

var _ = require('lodash');

module.exports = CreateUser;

CreateUser.$inject = ['apiv1', 'users', 'request', 'encrypt', 'decrypt', 'config'];
function CreateUser(server, User, request, encrypt, decrypt, config) {
  server.get('/participant/vm', function(req, res) {


    var username;
    var user;


    if (!req.user) {
      return res.sendStatus(403);
    }

    if (isVmPresent(req.user)) {
      return res.send(req.user.vm);
    }


    // function checkVM() {
    //   User.findById(req.user.id, function(e, user) {
    //     if (isVmPresent(user)) {
    //       return res.send(user.vm);
    //     } else {
    //       setTimeout(checkVM, 5000);
    //     }
    //   });
    //
    // }

    if (req.user.vm.pending) {
      // setTimeout(checkVM, 5000);
      return res.send({status: 'pending'});
    } else {
      // usernamen builden
      username = moniker.choose() + '-' + req.user.number;
      pollForVM();
    }
    function pollForVM() {

      // user holen
      User.getById(req.user.id)
        .then(function (resolvedUser) {
          user = resolvedUser;

          // flag für vm creation setzen "pending"
          user.editVM({pending: true})
            .then(function() {

              res.send({status: 'pending'});
            });

          var msg = {username: username};
          msg = encrypt(msg);

          request({
            url: config.backend.host + '/user/new',
            body: msg,
            json: true
          }, function (error, response, body) {
            if (error) {
              console.error('Something went wrong in the request: ', error);
            }

            // code 250 bedeutet für uns create windows user ist im gange... später nochmal nachfragen
            if (response.statusCode === 250) {
              setTimeout(pollForVM, 10000);
            } else {
              body = decrypt(body);

              console.log('New User was created!');
              console.log('Username: ' + body.username);
              console.log('Password: ' + body.password);

              user.editVM({username: body.username, password: body.password, pending: false});
            }
          });
        })
        .catch(function (e) {
          console.log(e);
          res.sendStatus(500)
        });
    }

    function isVmPresent(user) {
      return !_.isUndefined(_.get(user, 'vm.username')) && !_.isUndefined(_.get(user, 'vm.password'));
    }

  });
}

