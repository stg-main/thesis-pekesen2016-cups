'use strict';

module.exports = Client;

Client.$inject = ['linuxApi'];
function Client(server) {
  server.post('/participant/client', function(req, res) {

    if (!req.user) {
      return res.sendStatus(403);
    }

    req.user.setClientInfo(req.body.clientInfo, true)
      .then(function() {
        res.sendStatus(200);
      })
      .catch(function() {
        res.sendStatus(500);
      })

  });
}
