'use strict';

var _ = require('lodash');

module.exports = FinishFeedback;

FinishFeedback.$inject = ['linuxApi', 'users', 'request', 'encrypt', 'config'];
function FinishFeedback(server, User, request, encrypt, config) {
  server.post('/participant/feedback', function(req, res) {

    if (!req.user) {
      return res.sendStatus(403);
    }

    if (req.user.linux.finishedFeedback) {
      res.sendStatus(200);

    } else {
      var username;
      var user;
      
      // user holen
      User.getById(req.user.id)
        .then(function (resolvedUser) {
          user = resolvedUser;
          username = user.linux.vm.username;
          
          return resolvedUser.submitFeedback(req.body.feedback, true)
        })
        .then(function() {
          console.log('Participant \'' + req.user.email + '\' has submitted his feedback! (Linux)');
          res.sendStatus(200);
          
          var msg = {username: username};
          msg = encrypt(msg);

          request({
            url: config.backend.host + '/linux/user/end',
            body: msg,
            json: true
          }, function (error, response, body) {
            if (error) {
              console.error('Something went wrong in the request(Linux): ', error);
            }

            console.log('Participant \'' + req.user.email + '\' has ended its evaluation!');
            console.log('Linux VM User \'' + username + '\' has been deleted.');
          });
        })
        .catch(function (e) {
          console.log(e);
          res.sendStatus(500)
        });
    }

  });
}
