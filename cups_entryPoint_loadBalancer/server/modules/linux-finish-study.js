'use strict';

var _ = require('lodash');

module.exports = CreateUser;

CreateUser.$inject = ['linuxApi', 'users'];
function CreateUser(server, User) {
  server.post('/participant/end', function(req, res) {

    if (!req.user) {
      return res.sendStatus(403);
    }

    if (req.user.linux.finishedStudy) {
      res.sendStatus(200);

    } else {

      var username;
      var user;

      // user holen
      User.getById(req.user.id)
        .then(function (resolvedUser) {
          user = resolvedUser;
          username = user.linux.vm.username;

          // flag "finishedStudy setzen
          user.finishStudy(true)
            .then(function() {
              res.sendStatus(200);
            });
        })
        .catch(function (e) {
          console.log(e);
          res.sendStatus(500)
        });
    }

  });
}
