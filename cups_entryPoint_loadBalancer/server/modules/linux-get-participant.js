'use strict';

var Moniker = require('moniker');
var moniker = Moniker.generator([Moniker.adjective, Moniker.noun], {"maxSize":8});

var _ = require('lodash');

module.exports = CreateUser;

CreateUser.$inject = ['linuxApi', 'users', 'request', 'encrypt', 'decrypt', 'config'];
function CreateUser(server, User, request, encrypt, decrypt, config) {
  server.get('/participant/vm', function(req, res) {
    
    if (!req.user) {
      return res.sendStatus(403);
    }

    if (isVmPresent(req.user)) {
      return res.send(req.user.linux.vm);
    }


    function checkVM() {
      User.findById(req.user.id, function(e, user) {
        if (isVmPresent(user)) {
          return res.send(user.linux.vm);
        } else {
          setTimeout(checkVM, 5000);
        }
      });

    }

    if (req.user.linux.vm.pending) {
      setTimeout(checkVM, 5000);

    } else {

      var username;
      var user;

      // user holen
      User.getById(req.user.id)
        .then(function (resolvedUser) {
          user = resolvedUser;

          // flag für linux.vm creation setzen "pending"
          user.editVM({pending: true}, true);

          // usernamen builden
          username = moniker.choose() + '-' + user.number;

          var msg = {username: username};
          msg = encrypt(msg);

          request({
            url: config.backend.host + '/linux/user/new',
            body: msg,
            json: true
          }, function (error, response, body) {
            if (error) {
              console.error('Something went wrong in the request(Linux): ', error);
              return res.sendStatus(500);
            }

            body = decrypt(body);

            console.log('New User was created!');
            console.log('Username: ' + body.username);
            console.log('Password: ' + body.password);

            user.editVM({username: body.username, password: body.password, pending: false}, true)
              .then(function (user) {
                res.send(user.linux.vm);
              });
          });
        })
        .catch(function (e) {
          console.log(e);
          res.sendStatus(500)
        });
    }

    function isVmPresent(user) {
      return !_.isUndefined(_.get(user, 'linux.vm.username')) && !_.isUndefined(_.get(user, 'linux.vm.password'));
    }

  });
}

