'use strict';

module.exports = Logout;

Logout.$inject = ['apiv1'];

function Logout(server) {

  /**
   * @apiVersion 0.0.2
   *
   * @api {post} /api/v1/logout Logout
   * @apiDescription This route is for logging an user out.
   * @apiName Logout
   * @apiGroup Authorization
   *
   * @apiSuccess {200} Statuscode Returns 200 to indicate a successful logout.
   *
   * @apiUse NoPermissionError
   */

  server.post('/logout', function(req, res) {
    if (req.user) {
      req.logout();
      return res.sendStatus(200);
    } else {
      return res.sendStatus(403);
    }
  });
}
