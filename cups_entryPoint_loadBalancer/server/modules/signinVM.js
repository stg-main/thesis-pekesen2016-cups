'use strict';

var _ = require('lodash');

module.exports = LocalSignIn;

LocalSignIn.$inject = ['apiv1', 'passport', 'users'];
function LocalSignIn(server, passport, User) {

  /**
   * @apiVersion 0.0.2
   *
   * @api {post} /api/v1/signin Sign In
   *
   * @apiHeader {htmlForm} CONTENT-TYPE application/x-www-form-urlencoded.
   *
   * @apiParam {FormData} email User's email.
   * @apiParam {FormData} password User's password.
   *
   * @apiDescription This route is for signing in an user.
   * @apiName Sign In
   * @apiGroup Authorization
   *
   * @apiSuccess {200} Statuscode Returns 200 to indicate a successful Sign In.
   *
   * @apiUse NotFoundError
   * @apiUse MaxAttemptsError
   * @apiUse AlreadyLoggedInError
   * @apiUse InternalServerError
   *
   */

  server.post('/vm/signin', function(req, res, next) {
    passport.authenticate('local-signin', function(err, user, reason) {
      if (err) {
        return res.sendStatus(500);
      }

      // login was successful if we have a user
      if (user) {
        // handle login success
        return req.login(user, function(err) {
          if (err) return res.sendStatus(500);

          user.signInVM()
            .then(function() {
              user = {
                languages: user.languages,
                roles: user.roles,
                email: user.email,
                lastName: user.lastName,
                firstName: user.firstName
              };

              return res.send(user);
            });
        });
      }

      // otherwise we can determine why we failed
      var reasons = User.failedLogin;

      switch (reason) {
        case reasons.NOT_FOUND:
          return res.sendStatus(404);
          break;
        case reasons.PASSWORD_INCORRECT:
          // note: these cases are usually treated the same - don't tell
          // the user *why* the login failed, only that it did
          return res.sendStatus(404);
          break;
        case reasons.MAX_ATTEMPTS:
          // send email or otherwise notify user that account is
          // temporarily locked
          return res.sendStatus(460);
          break;
        case reasons.ALREADY_LOGGED_IN:
          return res.sendStatus(461);
          break;
        default:
          return res.sendStatus(500)
      }

    })(req, res, next);
  });

}
