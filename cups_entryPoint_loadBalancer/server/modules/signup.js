'use strict';

module.exports = LocalSignUp;

LocalSignUp.$inject = ['apiv1', 'passport', 'users'];
function LocalSignUp(server, passport, User) {

  /**
   * @apiVersion 0.0.2
   *
   * @api {post} /api/v1/signup Sign Up
   *
   * @apiHeader {htmlForm} CONTENT-TYPE application/x-www-form-urlencoded.
   *
   * @apiParam {FormData} email User's email.
   * @apiParam {FormData} password User's password.
   * @apiDescription This route is for an user registration.
   * @apiName Sign Up
   * @apiGroup Authorization
   *
   * @apiSuccess {200} Statuscode Returns 200 to indicate a successful Sign Up.
   *
   * @apiUse ConflictError
   * @apiUse InternalServerError
   */

  server.post('/signup', function(req, res, next) {
    passport.authenticate('local-signup', function(err, user, reason) {
      if (err) {
        // internal error
        return res.sendStatus(500);
      }

      if (user) {
        // handle success
        return req.login(user, function(err) {
          if (err) return res.sendStatus(500);

          return res.sendStatus(200)
        });
      } else {
        // otherwise we can determine why we failed
        var reasons = User.failedLogin;

        switch (reason) {
          case reasons.NOT_FOUND:
            return res.sendStatus(500);
            break;
          case reasons.DUPLICATE_EMAIL:
            return res.sendStatus(409);
            break;
          default:
            return res.sendStatus(500)
        }
      }

    })(req, res, next);
  });
}

