'use strict';

var express = require('express');

module.exports = UserState;

UserState.$inject = ['apiv1'];
function UserState(server) {
  server.get('/users/state', function(req, res) {
    if (req.user) {
      res.send(req.user);
    } else {
      res.sendStatus(403);
    }
  });
}
