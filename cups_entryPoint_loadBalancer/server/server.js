'use strict';

var http = require('http');
var morgan = require('morgan');
var express = require('express');
var bodyParser = require('body-parser');
var tiny = require('tiny-di');
var path = require('path');
var fsQ = require('q-io/fs');
var q = require('q');
var jsonfile = require('jsonfile');
var mongoose = require('mongoose');
var session = require('express-session');
var passport = require('passport');
var MongoStore = require('connect-mongo')(session);
var request = require('request');
var mongooseAutoIncrement = require('mongoose-auto-increment');
var NodeRSA = require('node-rsa');

var sessionSecret = require('./sessionsecret');


var bodySecret = 'C!s7FR-!E-_~VlWt^*CY)d-Mx6rm';

module.exports = Daemon;

function Daemon(logFunc, config) {

  var mongoUri = 'mongodb://' + config.databases.host +':' + config.databases.port + '/' + config.databases.db;

  var sessionStore = new MongoStore({url: mongoUri});
  var cookieOpts = {
    httpOnly: true,
    maxAge: config.session.maxAge
  };

  // Passport does not directly manage your session, it only uses the session.
  // So you configure session attributes (e.g. life of your session) via express
  var sessionOpts = {
    saveUninitialized: true, // saved new sessions
    resave: false, // do not automatically rewrite the session on every request to the session store
    store: sessionStore,
    secret: sessionSecret,
    cookie : cookieOpts
  };

  // Prepare dependency injection
  var $injector = new tiny();
  $injector.bind('$injector').to($injector);
  $injector.setResolver(dependencyResolver);

  var server = express();

  // log all requests to the console
  server.use(morgan('dev'));
  server.use(bodyParser.json());
  server.use(bodyParser.urlencoded({
    extended: true
  }));
  server.use(session(sessionOpts));
  server.use(passport.initialize());
  server.use(passport.session());

  // Setup APIv1 router
  var apiv1router = express.Router({'caseSensitive': true});
  apiv1router.use(bodyParser.json());
  apiv1router.use(bodyParser.urlencoded({
    extended: true
  }));
  server.use('/api/v1', apiv1router);

  // Setup linux router
  var linuxrouter = express.Router({'caseSensitive': true});
  linuxrouter.use(bodyParser.json());
  linuxrouter.use(bodyParser.urlencoded({
    extended: true
  }));
  server.use('/api/v1/linux', linuxrouter);


  // link injected variables
  $injector
    .bind('config').to(config)
    .bind('server').to(server)
    .bind('apiv1').to(apiv1router)
    .bind('linuxApi').to(linuxrouter)
    .bind('logFunc').to(logFunc)
    .bind('q').to(q)
    .bind('path').to(path)
    .bind('fsQ').to(fsQ)
    .bind('jsonfile').to(jsonfile)
    .bind('passport').to(passport)
    .bind('mongoose').to(mongoose)
    .bind('mongoUri').to(mongoUri)
    .bind('request').to(request)
    .bind('mongooseAutoIncrement').to(mongooseAutoIncrement)
    .bind('bodySecret').to(bodySecret)
    .bind('NodeRSA').to(NodeRSA);

  // Load all modules, specified in gulp/config.js (server.modules)
  loadExtensions();
  loadModules();

  runServer();

  function runServer() {
    var s = http.createServer(server);
    s.on('error', function(err) {
      if (err.code === 'EADDRINUSE') {
        if (isProd()) {
          logFunc('Development server is already started at port ' + config.server.prodPort);
        } else {
          logFunc('Development server is already started at port ' + config.server.devPort);
        }
      } else {
        throw err;
      }
    });

    if (isProd()) {
      s.listen(config.server.prodPort);
    } else {
      s.listen(config.server.devPort);
    }
  }

  function loadModules() {
    config.server.modules.forEach(function(module) {
      var file = module.file || module.module;
      $injector.bind(module.module).load(file);
    });
  }

  function loadExtensions() {
    config.server.extensions.forEach(function(extension) {
      var file = extension.file || extension.extension;
      $injector.bind(extension.extension).load(file);
    });
  }

  function dependencyResolver(moduleId) {
    var modulePath = path.resolve(path.join(config.dist.root, config.server.path, moduleId));
    try {
      return require(modulePath);
    } catch (e) {
      try {
        return require(moduleId);
      } catch (e2) {
        console.log('Extension ' + moduleId + ' failed to load');
        console.log(modulePath);
        console.log('errors', e, e2);
        console.log(new Error().stack);
        return false;
      }
    }
  }

  function isProd() {
    return process.env.NODE_ENV === 'production';
  }
}
