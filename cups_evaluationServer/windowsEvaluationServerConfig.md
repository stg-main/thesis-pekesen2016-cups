# konfiguration von windows server 2012

## remote desktop aktivieren (und user hinzufügen, bzw. den user in die richtige gruppe)
server-manager -> lokaler server -> remote desktop

## sessions einstellen
windows taste -> gpedit.msc -> computerkonfiguration -> windows-komponenten
-> remotedesktopdienste -> remotedesktopsitzungs-host -> verbindungen
=> remotedesktopdienste-benutzer auf eine remote... aktivieren
=> anzahl der verbindungen einschränken aktivieren und auf 999999 setzen

## Chocolatey + SHH + PowerShell
PowerShell Server v6 installieren
(get-carbon.org)

install chocolatey with following packages:
* sudo
* nano
* carbon
* dotnet
* powershell

create file C:\Windows\System32\WindowsPowerShell\v1.0\profile.ps1
in this file specify all startup scripts
* Import-Module 'Carbon'

## ReSharper
* install version 9.2 (latest 9.x) (sign in with can_ferit.pekesen@stud.tu-darmstadt.de)
* install it in "all user mode" -> https://www.jetbrains.com/resharper/help/Installation_Guide.html
* install KaVe Plugin
* every user that logs on has now resharper with kave plugin installed. they just have to start the evaluation period of resharper after starting visual studio

## user erstellen
window taste -> computerverwaltung -> lokale benutzer und gruppen -> benutzer
=> benutzer kann kennwort nicht ändern, kennwort läuft nie ab
=> mitglied von remotedesktopbenutzer
=> unter "sitzungen" getrennte sitzung nach 5 min beenden, zeitlimit 1 tag
    nach limit sitzung beenden, von beliebigem ort zulassen

## user management with powershell

### logoff one specific rdp session

`$server = 'CANPEKESEN28C3' (evtl in profile schreiben, da es sich nie ändert, rauskriegen durch get-user)
$username = 'username'

$session = ((quser /server:$server | ? { $_ -match $username }) -split ' +')[2]

logoff $session /server:$server`

### reset password

`$newCredentials = New-Credential -UserName 'username' -Password 'newPassword'
Install-User -Credential $newCredentials`

##### logoff all rdp users

`$server = 'CANPEKESEN28C3' #(evtl in profile schreiben, da es sich nie ändert, rauskriegen durch get-user)
$users = (quser /server:$server | ? { $_ -match 'rdp' })

ForEach($user in $users) {
  $sessionName = ($user -split ' +')[2];
  #evtl. hier auch reset pw möglich...
  logoff $sessionName /server:$server;
  }`

### create new Users

**Davor den Default User nach Belieben anpassen, damit Einstellungen und Ordner auch in den neuen User kopiert werden. Der Default User wird komplett als Vorlage für den neuen User genommen, der sich zum 1. Mal einloggt. Erst dann wird auch der User Folder für diesen erstellt!**
**Momentan haben wir auf dem Desktop die Visual Studio Verknüpfung, die Visual Studio Files in einem seperaten Ordner und AppData > Roaming > Microsoft > VisualStudio Ordner in den Default User kopiert, damit diese einem neuen User direkt zur Verfügung stehen -> noch notwendig nach all user mode?**

`$newUser = New-Credential -UserName 'username' -Password 'newPassword'
Install-User -Credential $newUser -UserCannotChangePassword -FullName
 'username'

Add-GroupMember -Name Benutzer -Member $newUser.Username
Add-GroupMember -Name Remotedesktopbenutzer -Member $newUser.Username
 `
