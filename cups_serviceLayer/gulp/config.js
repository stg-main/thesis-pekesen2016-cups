'use strict';
var serverconf = require('../js/config.json');

module.exports = {

  'devServer': {
    // if following files change node is being restarted
    'watchDirectory': 'js/',
    'watchIgnorePatterns': ['*.spec.js'],
    'server': 'js/start.js'
  }

};

// Add serverconfig to global config
module.exports.server = serverconf.server;
