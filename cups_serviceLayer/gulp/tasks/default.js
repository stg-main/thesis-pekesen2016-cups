'use strict';

var config  = require('../config');
var gulp    = require('gulp');
var forever = require('forever-monitor');

var child;

gulp.task('default', function() {

  child = forever.start(config.devServer.server, {
    args: ['./'],
    watch: true,
    watchDirectory: config.devServer.watchDirectory,
    watchIgnorePatterns: config.devServer.watchIgnorePatterns
  });

  if (process.env.NODE_ENV === 'production') {
    console.log('Server started on localhost on port ' + config.server.prodPort);
  } else if (process.env.NODE_ENV === 'productionTest') {
    console.log('Server started on localhost on port ' + config.server.prodTestPort);
  } else {
    console.log('Server started on localhost on port ' + config.server.devPort);
  }

  child.on('start', function() {
  });
  child.on('watch:restart', function() {
    console.log('restart server..');
  });

});

gulp.task('server:restart', function() {
  if (child) {
    child.restart();
  }
});
