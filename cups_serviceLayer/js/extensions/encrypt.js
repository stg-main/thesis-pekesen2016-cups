var pubFrontKey = '-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAk4E1mNnYpZK1BZ0pA5VkZafrTAaC9CibLr8Svj+/fYKJBjEK/t6bQm1+boD7\nAWQcIogyXynx7UnwPsDCwd2ll8Op/QIFoYARQ6RQUZFlb/uklk2ZSbdJ6woRyENmdFxFCll/\n1dcXYjtxemHYOCPayGOZbSeET0gUd/IPYA686ZjP9ku3yEbrSuIyXzI06TvH3iM6MtjclE9a\nTmUW6eOXAVC6uQPAV7kzK6yd7+mBd4WJ2uQBxxVOyNxEtftwqAheAj4XV2ZRBNSTexDpI3eW\nRQaJKRIouHRb02db5ns9eimy7e6o6inCY6zHVN/UTjpmZnbIkPubKGBVHARkTz6ebQIDAQAB\n-----END RSA PUBLIC KEY-----\n';

module.exports = Encrypt;

Encrypt.$inject = ['NodeRSA', 'bodySecret'];
function Encrypt(NodeRSA, bodySecret) {
  var encryptObject = NodeRSA(pubFrontKey);

  function encrypt(data) {
    data.secret = bodySecret;
    var msg = {
      data: data
    };
    msg.data = encryptObject.encrypt(msg.data, 'base64');

    return msg;
  }

  return encrypt;
}
