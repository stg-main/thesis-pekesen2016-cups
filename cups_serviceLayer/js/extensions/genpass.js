'use strict';

var crypto = require('crypto');

module.exports = GenKey;

GenKey.$inject = [];
function GenKey() {
  return function generateRandomKey(length) {
    length = length || 20;
    var chars = '2346789BCDFGHJKMPQRTVWXY';
    var charsetLength = chars.length;

    var randomBytes = crypto.randomBytes(length);
    var code = new Array(length);

    for (var j = 0; j < length; j++) {
      code[j] = chars[randomBytes[j] % charsetLength]
    }

    return code.join('');

  }
}
