module.exports = MongoConnection;

MongoConnection.$inject = ['logFunc', 'mongoose', 'mongoUri'];
// initial mongo setup
function MongoConnection(log, mongoose, mongoUri) {
  // connect to userDatabase
  var connection = mongoose.connect(mongoUri, function (err) {
    if (err) log(err);
  });

  return connection;
}
