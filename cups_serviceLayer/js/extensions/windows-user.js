module.exports = UserModel;

UserModel.$inject = ['config', 'q', 'mongoose'];
function UserModel(config, q, mongoose) {

  var Schema = mongoose.Schema;

  var UserSchema = new Schema({
      username: String,
      password: String,
      created: {type: Boolean, default: false}
    },
    {collection: 'windows-users'});

  UserSchema.statics.getByName = function(username) {
    var def = q.defer();

    this.findOne({username: username}, function(err, user) {
      if (err) {
        throw err;
      }
      if (user) {
        return def.resolve(user);
      } else {
        def.reject();
      }
    });

    return def.promise;
  };

  UserSchema.statics.savePassword = function(username, password) {
    var def = q.defer();

    this.findOne({username: username}, function(err, user) {
      if (err) {
        throw err;
      }
      if (user) {
        user.password = password;
        user.created = true;

        user.save(function(err) {
          if (err) return def.reject();
          return def.resolve(user);
        })
      } else {
        def.reject();
      }
    });

    return def.promise;
  };

  return mongoose.model('windows-users', UserSchema);
}
