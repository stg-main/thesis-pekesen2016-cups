'use strict';

var express = require('express');

module.exports = InstallLinuxUser;

InstallLinuxUser.$inject = ['server', 'ssh', 'encrypt', 'config', 'sshPrivKey', 'genPass'];
function InstallLinuxUser(server, ssh, encrypt, config, sshPrivKey, genPass) {
  server.get('/linux/user/new', function (req, res) {

    var username = req.body.username;
    var password = genPass(8);

    ssh.connect({
      host: config.linuxMachine.host,
      port: config.linuxMachine.port,
      username: config.linuxMachine.username,
      privateKey: sshPrivKey
    }).then(function conSuccess() {
      console.log('linux ssh connection was successfully established!');

      ssh.execCommand('useradd -m -G tsusers -p $(echo "' + password +  '" | openssl passwd -1 -stdin) ' + username)
        .then(function (result) {
          disconnect();
          console.log('linux create result: ', result);

          console.log('Linux User created!');
          console.log('Username: ' + username);
          console.log('Password: ' + password);
          var response = {
            username: username,
            password: password
          };
          response = encrypt(response);
          res.send(response);
        }, function cmdError(e) {
          disconnect();

          console.log('something went wrong while creating a linux user!');
          console.log(e);
          res.sendStatus(500);
        });
    }, function conReject(e) {
      console.log('linux ssh connection failed! ', e);
      res.sendStatus(500);
    });
  });

  function disconnect() {
    ssh.end();
  }
}
