'use strict';

var express = require('express');

module.exports = InstallUser;

InstallUser.$inject = ['server', 'ssh', 'encrypt', 'config', 'sshPrivKey', 'genPass', 'windowsUser'];
function InstallUser(server, ssh, encrypt, config, sshPrivKey, genPass, user) {
  server.get('/user/new', function (req, res) {

    var username = req.body.username;

    user.getByName(username)
      .then(function(user) {
        if (user.created) {
          var response = encrypt(user);
          return res.send(response);
        } else {
          return res.sendStatus(250);
        }
      })
      .catch(function() {
        res.sendStatus(250);
        createWindowsUser();
      });

    function createWindowsUser() {
      var newUser = new user({username: username});
      newUser.save();

      var password = genPass(8);

      ssh.connect({
        host: config.evalMachine.host,
        port: config.evalMachine.port,
        username: config.evalMachine.username,
        privateKey: sshPrivKey
      }).then(function conSuccess() {
        console.log('ssh connection was successfully established!');

        ssh.execCommand('powershell.exe .\\scripts\\createUser.ps1 -username ' + username + ' -password ' + password)
          .then(function (result) {
            // disconnect();
            console.log('windows create result: ', result);

            console.log('User created!');
            console.log('Username: ' + username);
            console.log('Password: ' + password);

            user.savePassword(username, password);

            // var response = {
            //   username: username,
            //   password: password
            // };
            // response = encrypt(response);
            // res.send(response);
          }, function cmdError(e) {
            // disconnect();

            console.error('something went wrong while creating a user!');
            console.error(e);
            // res.sendStatus(500);
          });
      }, function conReject(e) {
        console.error('ssh connection failed! ', e);
        // res.sendStatus(500);
      });
    }

  });

  function disconnect() {
    ssh.end();
  }
}
