'use strict';

var express = require('express');

module.exports = LogoffUser;

LogoffUser.$inject = ['server', 'ssh', 'config', 'sshPrivKey'];
function LogoffUser(server, ssh, config, sshPrivKey) {
  server.get('/linux/user/end', function (req, res) {

    var username = req.body.username;

    ssh.connect({
      host: config.linuxMachine.host,
      port: config.linuxMachine.port,
      username: config.linuxMachine.username,
      privateKey: sshPrivKey
    }).then(function conSuccess() {
      console.log('ssh connection was successfully established!');

      ssh.execCommand('pkill -KILL -u ' + username)
        .then(function (result) {
          console.log('linux user kill result: ', result);
          console.log('Linux User \'' + username + '\' logged off!');
        }, function cmdError(e) {
          console.log('something went wrong while logging off the linux user!');
          console.log('Username: ' + username);
          console.log(e);
          res.sendStatus(500);
        });

      ssh.execCommand('deluser --remove-home ' + username)
        .then(function (result) {
          disconnect();
          console.log('linux deluser result: ', result);

          console.log('Linux User \'' + username + '\' has been deleted');
          res.sendStatus(200);
        }, function cmdError(e) {
          disconnect();

          console.log('something went wrong while deleting the linux user');
          console.log('Username: ' + username);
          console.log(e);
          res.sendStatus(500);
        });
    }, function conReject(e) {
      console.log('linux ssh connection failed! ', e);
      res.sendStatus(500);
    });
  });

  function disconnect() {
    ssh.end();
  }
}
