'use strict';

var express = require('express');

module.exports = LogoffUser;

LogoffUser.$inject = ['server', 'ssh', 'config', 'sshPrivKey'];
function LogoffUser(server, ssh, config, sshPrivKey) {
  server.get('/user/end', function (req, res) {

    var username = req.body.username;
    var password = config.evalMachine.defaultPassword;

    ssh.connect({
      host: config.evalMachine.host,
      port: config.evalMachine.port,
      username: config.evalMachine.username,
      privateKey: sshPrivKey
    }).then(function conSuccess() {
      console.log('ssh connection was successfully established!');

      ssh.execCommand('powershell.exe .\\scripts\\logoffUser.ps1 -username ' + username)
        .then(function (result) {
          console.log('windows logoff result: ', result);

          console.log('User \'' + username + '\' logged off!');
        }, function cmdError(e) {
          console.log('something went wrong while logging off the user!');
          console.log('Username: ' + username);
          console.log(e);
          res.sendStatus(500);
        });

      ssh.execCommand('powershell.exe .\\scripts\\resetPassword.ps1 -username ' + username + ' -password ' + password)
        .then(function (result) {
          disconnect();
          console.log('windows reset result: ', result);

          console.log('User\'s \'' + username + '\' password got resetted to \'' + password + '\' !');
          res.sendStatus(200);
        }, function cmdError(e) {
          disconnect();

          console.log('something went wrong while resetting the password!');
          console.log('Username: ' + username);
          console.log(e);
          res.sendStatus(500);
        });
    }, function conReject(e) {
      console.log('ssh connection failed! ', e);
      res.sendStatus(500);
    });
  });

  function disconnect() {
    ssh.end();
  }
}
