'use strict';

var http = require('http');
var morgan = require('morgan');
var express = require('express');
var bodyParser = require('body-parser');
var tiny = require('tiny-di');
var path = require('path');
var fsQ = require('q-io/fs');
var q = require('q');
var node_ssh = require('node-ssh');
var ssh = new node_ssh();
var NodeRSA = require('node-rsa');
var fs = require('fs');
var createError = require('http-errors');
var decrypt = require('./extensions/decrypt')(NodeRSA);
var mongoose = require('mongoose');

var bodySecret = 'C!s7FR-!E-_~VlWt^*CY)d-Mx6rm';

module.exports = Daemon;

function Daemon(logFunc, config) {

  var mongoUri = 'mongodb://' + config.databases.host +':' + config.databases.port + '/' + config.databases.db;

  var sshPrivKey = fs.readFileSync(config.evalMachine.privateKey, 'utf8');

  // Prepare dependency injection
  var $injector = new tiny();
  $injector.bind('$injector').to($injector);
  $injector.setResolver(dependencyResolver);

  var server = express();

  // log all requests to the console
  server.use(morgan('dev'));
  server.use(bodyParser.json());
  server.use(bodyParser.urlencoded({
    extended: true
  }));
  server.use(function decryptBody(req, res, next) {
    try {
      req.body = decrypt(req.body);
      next();
    }
    catch(e) {
      console.log(e);
      next(createError(422, 'The body was malformed and therefore could not be decrypted.'));
    }
  });
  server.use(function checkSecret(req, res, next) {
    if (req.body.secret === bodySecret) {
      next();
    } else {
      next(createError(422, 'The secret provided in the body is wrong!'));
    }
  });


  // link injected variables
  $injector
    .bind('config').to(config)
    .bind('server').to(server)
    .bind('logFunc').to(logFunc)
    .bind('q').to(q)
    .bind('path').to(path)
    .bind('fsQ').to(fsQ)
    .bind('ssh').to(ssh)
    .bind('mongoUri').to(mongoUri)
    .bind('mongoose').to(mongoose)
    .bind('NodeRSA').to(NodeRSA)
    .bind('bodySecret').to(bodySecret)
    .bind('sshPrivKey').to(sshPrivKey);

  // Load all modules, specified in gulp/config.js (server.modules)
  loadExtensions();
  loadModules();

  runServer();

  function runServer() {
    var s = http.createServer(server);
    s.on('error', function(err) {
      if (err.code === 'EADDRINUSE') {
        if (isProd()) {
          logFunc('Development server is already started at port ' + config.server.prodPort);
        } else {
          logFunc('Development server is already started at port ' + config.server.devPort);
        }
      } else {
        throw err;
      }
    });

    if (isProd()) {
      s.listen(config.server.prodPort);
    } else {
      s.listen(config.server.devPort);
    }
  }

  function loadModules() {
    config.server.modules.forEach(function(module) {
      var file = module.file || module.module;
      $injector.bind(module.module).load(file);
    });
  }

  function loadExtensions() {
    config.server.extensions.forEach(function(extension) {
      var file = extension.file || extension.extension;
      $injector.bind(extension.extension).load(file);
    });
  }

  function dependencyResolver(moduleId) {
    var modulePath = path.resolve(path.join(config.dist.root, config.server.path, moduleId));
    try {
      return require(modulePath);
    } catch (e) {
      try {
        return require(moduleId);
      } catch (e2) {
        console.log('Extension ' + moduleId + ' failed to load');
        console.log(modulePath);
        console.log('errors', e, e2);
        console.log(new Error().stack);
        return false;
      }
    }
  }

  function isProd() {
    return process.env.NODE_ENV === 'production';
  }
}
